# README #

Python implementation of smooth isogeometric analysis on irregular meshes.

PyIGA is an open-source licensed under the BSD license.
Copyright (c) 2016, Thien Nguyen.
All rights reserved.

### How do I get set up? ###

   *  Install python vtk: sudo apt-get install python-vtk

   *  Install mayavi: sudo pip install mayavi

   *  Now, you are all set.